//Thomas Madigan
//9-18-18
//CSE 002
import java.util.Scanner; //imports scanner
// this program will take the area affected by a storm in acres and the amount of rainfall in inches and convert 
//those inputs into the quantity of rain in the form of cubic miles 
public class Convert{ //class
   	public static void main(String[] args) { //main method
      Scanner input = new Scanner ( System.in ); //configures scanner, allows input 
      System.out.print("Enter the affected area in acres xxxxx.xx: "); //allows for input of land size in acres
      double landAcres = input.nextDouble(); //creates variable for size of land in acres
      System.out.print("Enter the rainfall in the affected area xx: " ); //allows input for tip percentage
      double rainInches = input.nextDouble(); //creates variable for amount of rain
      double cubicInches =  landAcres * 6.273e+6 * rainInches; //takes land in acres and multiplies it by an integer to convert into square inches
      //then it multiplies the affected area in square inches by inches of rain to get cubic inches of rain
      double gallons = cubicInches * 0.004329; //converts cubic inches into gallons
      double cubicMiles = gallons * 9.08169e-13; //converts gallons into cubic miles of rain
      System.out.print ( +cubicMiles+ " cubic miles "); //calculates final output for cubic miles of rain from the storm
    }  //ends main method
} //end of class
