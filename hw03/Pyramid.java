//Thomas Madigan
//9-18-18
//CSE 002
import java.util.Scanner; //imports scanner
//this program will take the side length of the square face of a pyramid and the height of the pyramid
//and compute the volume of the three dimensional shape with those two inputs
public class Pyramid{ //class
   	public static void main(String[] args) { //main method
      Scanner input = new Scanner ( System.in ); //configures scanner, allows input 
      System.out.print("The square side of the pyramid is xx: "); //allows for input of square side length
      double squareLength = input.nextDouble(); //creates variable for square side length
      System.out.print("The height of the pyramid is xx: " ); //allows input for pyramid height
      double sideHeight = input.nextDouble(); //creates variable for pyramid height
      double volume = (Math.pow(squareLength, 2) * sideHeight) / 3; //plugs inputs into formula for pyramid volume
      System.out.println("The volume inside the pyramid is: " +Math.round(volume)); //displays result for volume computation
    } //end of main method
} //end of class