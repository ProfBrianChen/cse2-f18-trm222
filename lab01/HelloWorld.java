public class HelloWorld {
  
  public static void circleSwap(int[] in){
     int temp = 0;
     for (int i = 0; i < in.length; i++){
         temp = in[i];
         in[i] = in[i+1];
         in[i+1] = temp; 
     }
 }

public static void main(String args[]){
     int space1, space2, space3, space4, space5, space6, space7, space8, space9, space10;
        int[] array = new int[5]; 
        
        array[0] = 0;
        array[1] = 1;
        array[2] = 2;
        array[3] = 3;
        array[4] = 4;
        
    circleSwap(array);
    for (int j = 0; j < array.length; j++){
        System.out.print(array+" ");
    }
 }

 
 // public static void main (String args[]){
 //   System.out.print("Hello World");
//  }  
}