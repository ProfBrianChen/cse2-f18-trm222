import java.util.Scanner;

public class Hw05{
    
  public static void main(String[] args){
  //scanner is declared, first variables are declared for the initial input for number of loops
  Scanner input = new Scanner ( System.in ); 
  int count = 0;  //sets counter to 0
  int handNumb;
  String trash = ""; //throws away bad input
  int cardCount4 = 0;
  int cardCount3 = 0;
  int cardCount2 = 0;
  int cardCount1 = 0;
  int cardCount0 = 0;
  System.out.print("Enter number of hands to be generated: ");
  
  //will only allow for input of an int for number of loops
  while (input.hasNextInt()==false){
    trash = input.next();
    System.out.print("Please enter an integer: ");
  }
  handNumb = input.nextInt();
  
   //assigns random values to each card in a hand
    while (handNumb > count){  
    int card1 = (int) (Math.random()*51 + 1);
    int card2 = (int) (Math.random()*51 + 1);
    int card3 = (int) (Math.random()*51 + 1);
    int card4 = (int) (Math.random()*51 + 1);
    int card5 = (int) (Math.random()*51 + 1);
 //redraws card values if previouly drawn hands had repeating values
    while (card1==card2||card1==card3||card1==card4||card1==card5||card2==card3||card2==card4||card2==card5||card3==card4||card3==card5||card4==card5){
        card1 = (int) (Math.random()*51+1);
        card2 = (int) (Math.random()*51+1);
        card3 = (int) (Math.random()*51+1);
        card4 = (int) (Math.random()*51+1);
        card5 = (int) (Math.random()*51+1);
    }    
    //more variables for finding the remainder of each card value and the placeholders for the face value of each card
    int random1 = card1 % 13; 
    int random2 = card2 % 13;
    int random3 = card3 % 13;
    int random4 = card4 % 13;
    int random5 = card5 % 13;     
    int ace =0;
    int two =0;
    int three =0;
    int four=0;
    int five=0;
    int six=0;
    int seven=0;
    int eight=0;
    int nine =0;
    int ten=0;
    int jack=0;
    int queen=0;
    int king =0;
//these if statements are for deteriming the face value of each unique card per hand
        if (random1 == 0){
          king++;
        }
        else if (random1 == 1){
          ace++;
        }
        else if (random1 == 2){
          two++;
        }
        else if (random1 == 3){
          three++;
        }
        else if (random1 == 4){
          four++;
        }
        else if (random1 == 5){
          five++;
        }
        else if (random1 == 6){
          six++;
        }
        else if (random1 == 7){
          seven++;
        }
        else if (random1 == 8){
          eight++;
        }
        else if (random1 == 9){
          nine++;
        }
        else if (random1 == 10){
          ten++;
        }
        else if (random1 == 11){
          jack++;
        }
        else {
          queen++;
        }
       if (random2 == 0){
          king++;
        }
        else if (random2 == 1){
          ace++;
        }
        else if (random2 == 2){
          two++;
        }
        else if (random2 == 3){
          three++;
        }
        else if (random2 == 4){
          four++;
        }
        else if (random2 == 5){
          five++;
        }
        else if (random2 == 6){
          six++;
        }
        else if (random2 == 7){
          seven++;
        }
        else if (random2 == 8){
          eight++;
        }
        else if (random2 == 9){
          nine++;
        }
        else if (random2 == 10){
          ten++;
        }
        else if (random2 == 11){
          jack++;
        }
        else {
          queen++;
        }
       if (random3 == 0){
          king++;
        }
        else if (random3 == 1){
          ace++;
        }
        else if (random3 == 2){
          two++;
        }
        else if (random3 == 3){
          three++;
        }
        else if (random3 == 4){
          four++;
        }
        else if (random3 == 5){
          five++;
        }
        else if (random3 == 6){
          six++;
        }
        else if (random3 == 7){
          seven++;
        }
        else if (random3 == 8){
          eight++;
        }
        else if (random3 == 9){
          nine++;
        }
        else if (random3 == 10){
          ten++;
        }
        else if (random3 == 11){
          jack++;
        }
        else {
          queen++;
        }
       if (random4 == 0){
          king++;
        }
        else if (random4 == 1){
          ace++;
        }
        else if (random4 == 2){
          two++;
        }
        else if (random4 == 3){
          three++;
        }
        else if (random4 == 4){
          four++;
        }
        else if (random4 == 5){
          five++;
        }
        else if (random4 == 6){
          six++;
        }
        else if (random4 == 7){
          seven++;
        }
        else if (random4 == 8){
          eight++;
        }
        else if (random4 == 9){
          nine++;
        }
        else if (random4 == 10){
          ten++;
        }
        else if (random4 == 11){
          jack++;
        }
        else {
          queen++;
        }
       if (random5 == 0){
          king++;
        }
        else if (random5 == 1){
          ace++;
        }
        else if (random5 == 2){
          two++;
        }
        else if (random5 == 3){
          three++;
        }
        else if (random5 == 4){
          four++;
        }
        else if (random5 == 5){
          five++;
        }
        else if (random5 == 6){
          six++;
        }
        else if (random5 == 7){
          seven++;
        }
        else if (random5 == 8){
          eight++;
        }
        else if (random5 == 9){
          nine++;
        }
        else if (random5 == 10){
          ten++;
        }
        else if (random5 == 11){
          jack++;
        }
        else {
          queen++;
        }
     //this set of if statements compare the values of each counter per face card to eachother to determine the type of hand
    if (ace == 4 || two == 4 || three == 4 || four == 4 || five == 4 || six == 4 || seven == 4 || eight == 4 || nine == 4 || ten == 4 || jack == 4 || queen == 4 || king == 4){
        cardCount4++;     
    }  
    else if (ace == 3 || two == 3 || three == 3 || four == 3 || five == 3 || six == 3 || seven == 3 || eight == 3 || nine == 3 || ten ==3 || jack == 3 || queen == 3 || king == 3){   
        cardCount3++;
    }
    else if (ace <= 1 && two <= 1 && three <= 1 && four <=1 && five <= 1 && six <= 1 && seven <= 1 && eight <= 1 && nine <= 1 && ten <= 1 && jack <=1 && queen <= 1 && king <= 1){
        cardCount0++;
    }
    else if (ace == 2 && two != 2 && three != 2 && four != 2 && five != 2 && six != 2 && seven != 2 && eight != 2 && nine != 2 && ten != 2 && jack !=2 && queen != 2 && king != 2 ||
             two == 2 && ace != 2 && three != 2 && four != 2 && five != 2 && six != 2 && seven != 2 && eight != 2 && nine != 2 && ten != 2 && jack !=2 && queen != 2 && king != 2 ||
             three == 2 && two != 2 && ace != 2 && four != 2 && five != 2 && six != 2 && seven != 2 && eight != 2 && nine != 2 && ten != 2 && jack !=2 && queen != 2 && king != 2 ||
             four == 2 && two != 2 && three != 2 && ace != 2 && five != 2 && six != 2 && seven != 2 && eight != 2 && nine != 2 && ten != 2 && jack !=2 && queen != 2 && king != 2 ||
             five == 2 && two != 2 && three != 2 && four != 2 && ace != 2 && six != 2 && seven != 2 && eight != 2 && nine != 2 && ten != 2 && jack !=2 && queen != 2 && king != 2 ||
             six == 2 && two != 2 && three != 2 && four != 2 && five != 2 && ace != 2 && seven != 2 && eight != 2 && nine != 2 && ten != 2 && jack !=2 && queen != 2 && king != 2 ||
             seven == 2 && two != 2 && three != 2 && four != 2 && five != 2 && six != 2 && ace != 2 && eight != 2 && nine != 2 && ten != 2 && jack !=2 && queen != 2 && king != 2 ||
             eight == 2 && two != 2 && three != 2 && four != 2 && five != 2 && six != 2 && seven != 2 && ace != 2 && nine != 2 && ten != 2 && jack !=2 && queen != 2 && king != 2 ||
             nine == 2 && two != 2 && three != 2 && four != 2 && five != 2 && six != 2 && seven != 2 && eight != 2 && ace != 2 && ten != 2 && jack !=2 && queen != 2 && king != 2 ||
             ten == 2 && two != 2 && three != 2 && four != 2 && five != 2 && six != 2 && seven != 2 && eight != 2 && nine != 2 && ace != 2 && jack !=2 && queen != 2 && king != 2 ||
             jack == 2 && two != 2 && three != 2 && four != 2 && five != 2 && six != 2 && seven != 2 && eight != 2 && nine != 2 && ten != 2 && ace !=2 && queen != 2 && king != 2 ||
             queen == 2 && two != 2 && three != 2 && four != 2 && five != 2 && six != 2 && seven != 2 && eight != 2 && nine != 2 && ten != 2 && jack !=2 && ace != 2 && king != 2 ||
             king == 2 && two != 2 && three != 2 && four != 2 && five != 2 && six != 2 && seven != 2 && eight != 2 && nine != 2 && ten != 2 && jack !=2 && queen != 2 && ace != 2){
        cardCount1++;
    } 
    else {
        cardCount2++;
    }  
    count = count + 1; //increases counter after every completed hand
    }
    //calculates probability of each possible hand and rounds to three decimal places
    int total = cardCount4 + cardCount3 + cardCount2 + cardCount1 + cardCount0;  
    double final4 = (double) cardCount4 / total;
    final4 = Math.round(final4 * 1000d) / 1000d;    
    double final3 = (double) cardCount3 / total;
    final3 = Math.round(final3 * 1000d) / 1000d;    
    double final2 = (double) cardCount2 / total;
    final2 = Math.round(final2 * 1000d) / 1000d;   
    double final1 = (double) cardCount1 / total;  
    final1 = Math.round(final1 * 1000d) / 1000d;
    double final0 = (double) cardCount0 / total;
    final0 = Math.round(final0 * 1000d) / 1000d;
  //prints the probabilities of each hand based on the results and prints the number of inputted loops
 System.out.println("number of loops: " +handNumb);
 System.out.println("probability of 4 of a kind: " +final4);
 System.out.println("probability of 3 of a kind: " +final3);
 System.out.println("probability of 2 pairs: " +final2);
 System.out.println("probability of 1 pair: " +final1);
 System.out.println("probability of five different cards: " + final0);  
      
 }
}




