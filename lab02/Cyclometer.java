//Thomas Madigan
//9-6-2018
//CSE 002 
//this program will take the elasped time and number of front wheel rotations on a bicycle and will then output the length of the trip in minutes,
//the number of counts for each trip, the distance of each trip in miles, and the combined distance of both trips in miles. 
public class Cyclometer {
    public static void main(String[] args) {
      // inputs
      int secsTrip1=480; //time for first trip in seconds
      int secsTrip2=3220; //time for second trip in seconds
      int countsTrip1=1561; //number of front wheel rotations for first trip
      int countsTrip2=9037; //number of front wheel rotations for second trip 
      double wheelDiameter=27.0,  // value for the diameter of the bike wheels
  	  PI=3.14159, // value of pi
      feetPerMile=5280,  // number of feet in every mile
  	  inchesPerFoot=12,   // number of inches in every foot
  	  secondsPerMinute=60;  // number of seconds per minute 
	    double distanceTrip1, distanceTrip2,totalDistance;  // declares both trip distances and the total distance as doubles
      //main method standard for all java programs
        System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+ " counts."); //prints out the time for trip 1 in minutes and number of counts
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts."); //prints out the time for trip 2 in minutes and number of counts
         distanceTrip1=countsTrip1*wheelDiameter*PI;
        //above gives distance in inches
       	//(for each count, a rotation of the wheel travels
       	//the diameter in inches times PI)
    	distanceTrip1/=inchesPerFoot*feetPerMile; //computes distance in miles for trip 1
    	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //computes distance in miles for trip 2 
    	totalDistance=distanceTrip1+distanceTrip2; //computes total distance
	    //print out the output data.
        System.out.println("Trip 1 was "+distanceTrip1+" miles"); //prints trip 1 distance in miles
	      System.out.println("Trip 2 was "+distanceTrip2+" miles"); //prints trip 2 distance in miles
    	  System.out.println("The total distance was "+totalDistance+" miles"); //prints total distance in miles
      } //end of main cycle   
} //end of class