//Tom Madigan
//9-25-18
import java.util.Scanner;
//this program will simulate rolling two dice and will output the common name for each roll based on the game craps
//users can either input their own die values or allow java to select them randomly.
public class CrapsIf{ //class
  
  public static void main(String args[]){ //main
    Scanner question = new Scanner ( System.in ); //scanner for selecting random and inout option
    Scanner roll = new Scanner ( System.in ); //scanner for input option
    System.out.print("to choose two dice to evaluate type '1' or for two random rolls type '2':");
    double choice = question.nextDouble(); //takes input for either random or manual input
    //program allows for manual input of dice values
    if ( choice == 1 ){ //first if statement for selecting the input option
    System.out.print("first dice value: "); //input for dice 1
    int diceOne = roll.nextInt(); //assigns value to dice 1
    System.out.print("second dice value: "); //input for dice 2
    int diceTwo = roll.nextInt(); //assigns value to dice 2
    double inputTotal = diceOne + diceTwo; //adds both values together
      //this chunk of if statements looks at the combined value of the dice and use boolean logic to determine the name of each roll
      if ( inputTotal == 2 ){
        System.out.print("Snake Eyes");
      }
      if ( inputTotal == 3 ){
        System.out.print("Ace Deuce");
      }
      if ( inputTotal == 4 && diceOne == 2 ){
        System.out.print("Hard Four");
      }
      if ( inputTotal == 4 && diceOne != 2 ){
        System.out.print("Easy Four");
      }
      if ( inputTotal == 5 ){
        System.out.print("Fever Five");
      }
      if ( inputTotal == 6 && diceOne == 3 ){
        System.out.print("Hard Six");
      }
      if ( inputTotal == 6 && diceOne != 3 ){
        System.out.print("Easy Six");
      }
      if ( inputTotal == 7 ){
        System.out.print("Seven Out");
      }
      if ( inputTotal == 8 && diceOne == 4 ){
        System.out.print("Hard Eight");
      }
      if ( inputTotal == 8 && diceOne != 4 ){
        System.out.print("Easy Eight");
      }
      if ( inputTotal == 9 ){
        System.out.print("Nine");
      }
      if ( inputTotal == 10 && diceOne ==5 ){
        System.out.print("Hard Ten");
      }
      if ( inputTotal == 10 && diceOne != 5 ){
        System.out.print("Easy Ten");
      }
      if ( inputTotal == 11 ){
        System.out.print("Yo-leven");
      }
      if ( inputTotal == 12 ){
        System.out.print("Boxcars");
      }   
    } 
    //program randomly selects dice values and produces names
    else {
      double randomNumber1 = (int) (Math.random() * 6) + 1; //randomly selects value for one die
      double randomNumber2 = (int) (Math.random() * 6) + 1; //randomly selects value for other die
      double rollTotal = randomNumber1 + randomNumber2; //combines the value of each die
      //these if statements use boolean logic along with the combined values of the rolls to determine the common name of each roll
      if ( rollTotal == 2 ){
        System.out.print("Snake Eyes");
      }
      if ( rollTotal == 3 ){
        System.out.print("Ace Deuce");
      }
      if ( rollTotal == 4 && randomNumber1 == 2 ){
        System.out.print("Hard Four");
      }
      if ( rollTotal == 4 && randomNumber1 != 2 ){
        System.out.print("Easy Four");
      }
      if ( rollTotal == 5 ){
        System.out.print("Fever Five");
      }
      if ( rollTotal == 6 && randomNumber1 == 3 ){
        System.out.print("Hard Six");
      }
      if ( rollTotal == 6 && randomNumber1 != 3 ){
        System.out.print("Easy Six");
      }
      if ( rollTotal == 7 ){
        System.out.print("Seven Out");
      }
      if ( rollTotal == 8 && randomNumber1 == 4 ){
        System.out.print("Hard Eight");
      }
      if ( rollTotal == 8 && randomNumber1 != 4 ){
        System.out.print("Easy Eight");
      }
      if ( rollTotal == 9 ){
        System.out.print("Nine");
      }
      if ( rollTotal == 10 && randomNumber1 ==5 ){
        System.out.print("Hard Ten");
      }
      if ( rollTotal == 10 && randomNumber1 != 5 ){
        System.out.print("Easy Ten");
      }
      if ( rollTotal == 11 ){
        System.out.print("Yo-leven");
      }
      if ( rollTotal == 12 ){
        System.out.print("Boxcars");
      }   
    } 
  } //end main
} //end class