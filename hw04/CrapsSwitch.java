//Tom Madigan
//9-25-18
import java.util.Scanner;
//this program will simulate rolling two dice and will output the common name for each roll based on the game craps
//users can either input their own die values or allow java to select them randomly. All this with only switch statements.
public class CrapsSwitch{ //class
  
  public static void main(String args[]){ //main
    Scanner question = new Scanner ( System.in ); //scanner for selecting random and inout option
    Scanner roll = new Scanner ( System.in ); //scanner for input option
    System.out.print("to choose two dice to evaluate type '1' or for two random rolls type '2':");
    int choice = question.nextInt(); //takes input for either random or manual input
    //program allows for manual input of dice 
    //main switch statement which breaks off into more specific switches in order to identify roll name
    switch ( choice ){ //first if statement for selecting the input option
      case 1:
        System.out.print("first dice value: "); //input for dice 1
        int diceOne = roll.nextInt(); //assigns value to dice 1
        System.out.print("second dice value: "); //input for dice 2
        int diceTwo = roll.nextInt(); //assigns value to dice 2
        int inputTotal = diceOne + diceTwo; //adds both values together
        //this chunk of switch statements looks at the combined value of the dice and determine the name of each roll using the value of one roll
       switch ( inputTotal ){
        case 2:
          System.out.print("Snake Eyes");
          break;
        case 3:
          System.out.print("Ace Deuce");
          break;
        case 4:
          switch ( diceOne ){
            case 2:
              System.out.print("Hard Four");
              break;
            default:
              System.out.print("Easy Four");
              break;
          }
           break;
        case 5:
          System.out.print("Fever Five");
          break;
        case 6:
          switch ( diceOne ){
            case 3:
              System.out.print("Hard Six");
              break;
            default:
              System.out.print("Easy Six");
              break;
          }
           break;
        case 7:
          System.out.print("Seven Out");
          break;
        case 8:
          switch ( diceOne ){
            case 4: 
              System.out.print("Hard Eight");
              break;
            default: 
              System.out.print("Easy Eight");
              break;
          }
           break;
        case 9:
          System.out.print("Nine");
          break;
        case 10:
          switch ( diceOne ){
            case 5: 
              System.out.print("Hard Ten");
              break;
            default: 
              System.out.print("Easy Ten");
              break;
          }
           break;
        case 11:
          System.out.print("Yo-leven");
          break;
        case 12:
          System.out.print("Boxcars");
          break;
       }
        break;
      case 2:
        int randomNumber1 = (int) (Math.random() * 6) + 1; //randomly selects value for one die
        int randomNumber2 = (int) (Math.random() * 6) + 1; //randomly selects value for other die
        int rollTotal = randomNumber1 + randomNumber2; //combines the value of each die
    switch ( rollTotal ){  //this chunk of switch statements looks at the combined value of the dice and determine the name of each roll using the value of one roll
        case 2:
          System.out.print("Snake Eyes");
          break;
        case 3:
          System.out.print("Ace Deuce");
          break;
        case 4:
          switch ( randomNumber1 ){
            case 2:
              System.out.print("Hard Four");
              break;
            case 1:
              System.out.print("Easy Four");
              break;
            case 4:
              System.out.print("Easy Four");
              break;
          }
        break;
        case 5:
          System.out.print("Fever Five");
          break;
        case 6:
          switch ( randomNumber1 ){
            case 3:
              System.out.print("Hard Six");
              break;
            case 2:
              System.out.print("Easy Six");
              break;
            case 4:
              System.out.print("Easy Six");
              break;
          }
        break;
        case 7:
          System.out.print("Seven Out");
          break;
        case 8:
          switch ( randomNumber1 ){
            case 4: 
              System.out.print("Hard Eight");
              break;
            case 2: 
              System.out.print("Easy Eight"); 
              break;
            case 3: 
              System.out.print("Easy Eight"); 
              break;
            case 5: 
              System.out.print("Easy Eight"); 
              break;
            case 6: 
              System.out.print("Easy Eight"); 
              break;
          }
        break;
        case 9:
          System.out.print("Nine");
          break;
        case 10:
          switch ( randomNumber1 ){
            case 5: 
              System.out.print("Hard Ten");
              break;
            case 4: 
              System.out.print("Easy Ten");
              break;
            case 6: 
              System.out.print("Easy Ten");
              break;
          }
        break;
        case 11:
          System.out.print("Yo-leven");
          break;
        case 12:
          System.out.print("Boxcars");     
       }
    }
  } //end main
} //end class
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  