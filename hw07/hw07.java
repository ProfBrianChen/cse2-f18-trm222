//Tom Madigan
//CSE2

import java.util.Scanner; //imports scanner

public class hw07 {
    
    public static String sampleText(){ //method for user input in form of a string
      Scanner input = new Scanner(System.in); //creates scanner
      String words = ""; //initializes variable
      System.out.print("Please enter text: "); //prompts user for input
      words = input.nextLine(); //assigns next line of input to variable
      System.out.println("You entered: " +words); //outputs the users input
       return  words; //returns the input
    }

	public static String printMenu(){ //method for creating a menu
	
	String menu = "MENU \n c - Number of non-whitespace characters \n w - Number of words \n f - Find Text \n r - Replace all !s \n s - Shorten spaces \n q - Quit \n Choose an option: "; //prints out menu
	
	return menu; //returns 
	
	}





    public static void main (String[] args){ //main method
       Scanner input = new Scanner(System.in);
       sampleText(); 
       System.out.println(printMenu()); //calls from the previous methods
      String e = input.next();
      int r = 1;
      
      if ( e == "c" ){
        r = 1;
      }
      else if ( e == "w"){
        r = 2;
      }
      else if ( e == "f"){
        r = 3;
      }
      else if ( e == "r"){
        r = 4;
      }
      else if ( e == "s"){
        r = 5;
      }
      else {
        r = 6;
      }
        
        
        
        
      switch( r ) {
        case 1: 
          System.out.println("w");
          break;
        case 2:
          System.out.println("zzzzzzzzz");
          break;
        case 3:
          System.out.println("tttt");
          break;
        case 4:
          System.out.println("dinner");
          break;
        case 5:
          System.out.println("beep beep");
          break;
        default:
          System.out.println("default");
          break;
    }
      
    } //end main
} //end class
