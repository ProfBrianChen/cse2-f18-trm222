//Tom Madigan
//CSE2

import java.util.Scanner;

public class hw08{ 
  
//This method produces a hand of five unique cards and stores them into an array, it returns 
  public static String[] getHand(String[] array1, int index,  int numCards) { 
      //initializes variables for cards in hand
        int card1;
        int card2;
        int card3;
        int card4;
        int card5; 
        String[] hand = new String[5]; //declares an array
        
        //This chunk of code selects five unique random numbers for each hand
        card1 = (int) (Math.random() * 52);
        card2 = (int) (Math.random() * 52);
        while (card1==card2) 
            {
               card2 = (int) (Math.random() * 52);
            }
        card3 = (int) (Math.random() * 52);
        while(card1==card3 || card2==card3)
            {
               card3 = (int) (Math.random()*52);
            }
        card4 = (int) (Math.random()*52);
        while(card1==card4 || card2==card4 || card3==card4)
            {
               card4 = (int) (Math.random()*52);
            }
        card5 = (int) (Math.random()*52);
        while(card1==card5 || card2==card5 || card3==card5 || card4==card5)
            {
               card5 = (int) (Math.random()*52);
            }
        
        //The values of each card in a hand are assigned to an array
        hand[0] = array1[card1];
        hand[1] = array1[card2];
        hand[2] = array1[card3];
        hand[3] = array1[card4];
        hand[4] = array1[card5];
        return hand; //returns the array 
    }
  
  public static void printArray(String[] array2) { 
        String printAll = ""; 
        int i = 0;
        for(i = 0; i < array2.length; i++) { //for loop that prints all 52 cards in a deck
            printAll = printAll + (array2[i]+" ");
        }
        System.out.println(printAll); 
    }
  
  public static String[] shuffle(String[] array3) { 
        int i = 0;
        for (i = 0; i < 52; i++) {	//for loop that shuffles the 52 cards by randomizing the index of the array
	        int shuf = (int) (Math.random()*52); 
	        String placeholder = array3[shuf];
	        array3[shuf] = array3[i];
	        array3[i] = placeholder;
        }
        return array3; 
    }

public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 
System.out.println("");
//printArray(cards); 
shuffle(cards); 
printArray(cards); 
System.out.println("Shuffled");
while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   printArray(hand);
   System.out.println("Hand");
   index = index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  } 
}
