import java.util.Random;

public class lab07{


     public static String Adjectives(){
        
       Random randomGenerator = new Random();
       int randomInt = randomGenerator.nextInt(10);
        String adjective = ""; 
          switch ( randomInt ) { 
          
          case 1:
            adjective = "spicy";
              break;
          case 2:
            adjective = "ugly";
              break;
          case 3:
            adjective = "fat";
              break;
           case 4:
            adjective = "tired";
              break;
           case 5:
            adjective = "sweaty";
              break;
           case 6:
            adjective = "old";
              break;
           case 7:
            adjective = "crazy";
              break;
           case 8:
            adjective = "handsome";
              break;
           default:
            adjective = "honest";
              break;
     }
       return adjective;
}
  
       public static String SubjectNouns(){
        
       Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        String subject = "";
          switch ( randomInt ) { 
          
          case 1:
            subject = "bulldozer";
              break;
          case 2:
            subject = "car";
              break;
          case 3:
            subject = "computer";
              break;
           case 4:
            subject = "robot";
              break;
           case 5:
            subject = "pumpkin";
              break;
           case 6:
            subject = "tree";
              break;
           case 7:
            subject = "library";
              break;
           case 8:
            subject = "water bottle";
              break;
           default:
            subject = "window";
              break;
     }
         return subject;
}
  
       public static String ObjectNouns(){
        
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        String object = "";
          switch ( randomInt ) { 
          
          case 1:
            object = "lion";
              break;
          case 2:
            object = "witch";
              break;
          case 3:
            object = "duck";
              break;
           case 4:
            object = "man";
              break;
           case 5:
            object = "woman";
              break;
           case 6:
            object = "doctor";
              break;
           case 7:
            object = "hawk";
              break;
           case 8:
            object = "turkey";
              break;
           default:
            object = "bear";
              break;
     }
         return object;
}
  
       public static String Verbs(){
        
       Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        String verb = "";
          switch ( randomInt ) { 
          
          case 1:
            verb = "threw";
              break;
          case 2:
            verb = "jumped";
              break;
          case 3:
            verb = "kicked";
              break;
          case 4:
            verb = "punched";
              break;
          case 5:
            verb = "fought";
              break;
          case 6:
            verb = "tripped";
              break;
          case 7:
            verb = "avoided";
              break;
           case 8:
            verb = "slapped";
              break;
           default:
            verb = "bodyslammed";
              break;
            
     }
         return verb; 
}
  public static void main(String args[]){
    
  String theObject = ObjectNouns();
  String sentence = "The " +Adjectives()+ " " +theObject+ " " +Verbs()+ " the " +Adjectives()+ " " +SubjectNouns();
  String sentence2 = "It " +Verbs()+ " " +Adjectives()+ " " +SubjectNouns()+ " at the " +Adjectives()+ " " +ObjectNouns();
  String sentence3 = "That " +theObject+ " " +Verbs()+ " her " +SubjectNouns();
  System.out.println(sentence);
  System.out.println(sentence2);
  System.out.println(sentence3);
    
  }
}