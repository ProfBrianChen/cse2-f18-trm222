//Thomas Madigan
//9-13-18
//CSE 002
import java.util.Scanner; //imports scanner and allows for manual input in terminal
// this program will take the input of original cost of a check, the desired tip percentage, 
//take the number of ways it must be split and output the amount that must be paid per person
public class Check{
   	public static void main(String[] args) {
      Scanner myScanner = new Scanner ( System.in ); //sets up scanner, allows input 
      System.out.print("Enter the original cost of the check in the form xx.xx: "); //allows for input of orginal cost
      double checkCost = myScanner.nextDouble(); //creates variable for original cost input
      System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " ); //allows input for tip percentage
      double tipPercent = myScanner.nextDouble(); //creates variable for tip percentage 
      tipPercent /= 100; //converts the percentage into a decimal value
      System.out.print("Enter the number of people who went out to dinner: "); //allows input for number of people
      int numPeople = myScanner.nextInt(); //creates variable for number of people
      double totalCost;
      double costPerPerson;
      int dollars,   //whole dollar amount of cost 
            dimes, pennies; //for storing digits to the right of the decimal point for cost 
      totalCost = checkCost * (1 + tipPercent);
      costPerPerson = totalCost / numPeople; //gets the whole amount, eliminates fraction
      dollars=(int)costPerPerson; //gets dollar amount per person
      dimes=(int)(costPerPerson * 10) % 10; //gets dime amount, % returns remainder
      pennies=(int)(costPerPerson * 100) % 10; //gets penny amount, % returns remainder
      System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); //calculates final output for cost per person
    }  //end of main method   
} //end of class
