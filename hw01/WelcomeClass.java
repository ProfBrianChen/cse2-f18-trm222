public class WelcomeClass {
  
  public static void main (String[]args){
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-T--R--M--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("My name is Thomas Madigan but I go by Tom.");
    System.out.println("I'm from Bethlehem, PA and I plan on studying");
    System.out.println("finance and either minoring in cs or transferring");
    System.out.println("into csb. In my free time I enjoy swimming,");
    System.out.println("golfing, gaming, going to the movies, and hanging");
    System.out.println("out with friends.");
  }
}