//Tom Madigan
//CSE2

import java.util.Scanner;

public class RemoveElements {

 public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }

 public static int[] randomInput() { //method that creates random array 
	
        int space1, space2, space3, space4, space5, space6, space7, space8, space9, space10; //intializes placeholders
        int[] array = new int[10];  //creates new array of size 10
        
        //assigns each placeholder a random value
        space1 = (int) (Math.random() * 9); 
        space2 = (int) (Math.random() * 9); 
        space3 = (int) (Math.random() * 9);
        space4 = (int) (Math.random() * 9);
        space5 = (int) (Math.random() * 9);
        space6 = (int) (Math.random() * 9);
        space7 = (int) (Math.random() * 9);
        space8 = (int) (Math.random() * 9);
        space9 = (int) (Math.random() * 9);
        space10 = (int) (Math.random() * 9);
        
        //assigns each placeholder to corresponding location in array
        array[0] = space1;
        array[1] = space2;
        array[2] = space3;
        array[3] = space4;
        array[4] = space5;
        array[5] = space6;
        array[6] = space7;
        array[7] = space8;
        array[8] = space9;
        array[9] = space10;
        
        return array; //returns array
 } //end method
        
 public static int[] delete(int[] array, int index) { //method that deletes certain index of array
     
   int [] newArray = new int[array.length - 1]; //creates new array with one less index than randomly generated array
  
   for (int j = 0; j < index; j++) {
     newArray[j] = array[j]; //for loop assigns array values to new array
   }
 
   for (int i = index + 1; i < newArray.length; i++) { //for loop removes desired index from array 
     newArray[index] = array[i]; 
     index += 1; //increments index number
   }
   return newArray; //returns new array
 } //end method

 public static int[] remove(int[] array, int target) { //method that removes all of one value from an array 

   int count = 0; //counter
   
   for (int i = 0; i < array.length; i++) { 
     if (array[i] > target || array[i] < target) { //runs if the target value is not equal to current array value
       count += 1; //increments counter
     }
   }
  
   int newArray[] = new int[count]; //creates new array
   int newCount = 0; //second counter
  
   for (int j = 0; j < array.length; j++) {
     if (array[j] > target || array[j] < target) //passes through if array value is not equal to target
     {
       newArray[newCount] = array[j]; //asigns array value to new array
       newCount += 1; //increments second counter
     }
   }
   return newArray; //returns the new array
 } //end method
} //end class
