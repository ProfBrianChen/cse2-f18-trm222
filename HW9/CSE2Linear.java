//Tom Madigan
//CSE2

import java.util.Scanner;
import java.util.Arrays;

public class CSE2Linear { //class
    
    public static void main(String[]args){ //main method
        
        System.out.print("Enter 15 ascending ints for final grades in CSE2: "); //prompts user for input
        System.out.println(); 
        Scanner input = new Scanner(System.in); //initializes scanner
        int count = 0; //initializes counter
        int[] array = new int[15]; //creates an array with an index of 15
        while (count <= 14) //creates loop that will run 15 times
        {
            if (input.hasNextInt()) //passes through if user input is an int
            {
                array[count] = input.nextInt(); //assigns the int to corresponding array location
                if (array[count] >= 101 || array[count] <= -1){ //loop for ints that are outside of range
                    System.out.println("Error, please enter only ints from 0-100"); //error message
                    return; //stops program
                }
                else { //passes through if int is within range
                    if (count > 0) { //starts after first iteration
                        if (array[count] < array[count - 1]) { //creates error is ints are not entered in ascending order
                            System.out.println("Error, ints must be entered in ascending order"); //error message
                            return; //stops program
                        }
                    }
                count += 1; //increments counter
                }
            }
            else { //situation where an int was not entered
                System.out.println("Error, you must enter ints only"); //error message
                return; //stops program
            }
        }
        for (int i = 0; i < array.length; i++){ //for loop to print out array
            System.out.print(array[i]+ " ");
        }
        System.out.println("Enter a grade to search for: "); //prompts user to enter grade to search for
        if (input.hasNextInt()) { 
            int grade = input.nextInt(); //assigns desired grade to be searched for to a var
            binary(array, grade); //calls the binary search method
            scramble(array); //calls scramble method
            System.out.println("Scrambled:"); //tells user that array is scrambled
                for (int i = 0; i < array.length; i++) { //for loop to print scrambled array
                     System.out.print(array[i]+ " ");
                }
            System.out.print("Enter a grade to search for: "); //prompts user for another grade 
            if (input.hasNextInt()) { //passes through if user enters int
                linear(array, grade); //calls linear search method
            }
            else {
                System.out.println("Error, you must only enter an int"); //error message if int is not entered
                return; //ends program
            }
        }
        else {
            System.out.println("Error, grade not found"); //error message if grade is not found in array
            return; //ends program
        }
    } //end main
    
    public static void linear(int[] array, int grade) { //linear search method
        for (int i = 0; i < array.length; i++) { //for loop to go through values of the array
            if (array[i] == grade) { //if the array value is equal to grade being searched
                System.out.println(grade + " was found in the list in " + (i + 1) + " iterations"); //prints this if true
                break;
            }
            else if (array[i] != grade && i == (array.length - 1)) { //runs if grade is not found in array
                System.out.println(grade + " was not found in " + (i + 1) + " iterations"); //prints message if grade was not found
            } 
        }
    } //end method
    
     public static void binary(int[] array, int grade) { //binary search method
        
        int high = array.length - 1; //declares high index as teh highest index val in array
        int low = 0; //declares low index as lowest array index
        int count = 0; //counter set to 0
        while (low <= high) { //loop that runs until high and low are equal or low is greater
            count+= 1; //increments count
            int mid = (high + low) / 2; //declares the middle index to be the average of high and low
            if (grade > array[mid]) { //loop that checks if the grade being searched is above the array value with index of mid
                low = mid + 1; //changes value of low
            }
             else if (grade < array[mid]) { //checks if grade being searched is below mid
                high = mid - 1; //lowers value of high
            }
            else if (grade == array[mid]){ //checks if grade is equal to mid index value
                System.out.println(grade+ " was found in " +count+ " iterations"); //prints out message with number of iterations through the loop until found
                break; //breaks
            }
            else if (low > high) { //loop that runs if grade is not found inside array
            System.out.println(grade+ " was not found in " +count+ " iterations"); //prints out message that grade was not found 
       }
      } 
     } //end method
    
    public static int[] scramble(int[] array) { //scramble method
        for (int i = 0; i < array.length; i++) { //for loop to mix every value in the array
            int rand = (int)(Math.random()*array.length); //creates random index to replace current index
            int temp; //declares temporary placeholder
            temp = array[i]; //array value is stored in temp
            while (rand < i || rand > i) { //loop runs if the random index value is different frim the current value
                array[i] = array[rand]; //array val of random is moved to other array value
                array[rand] = temp; //old array value is moved to temp
                break; //breaks
            }
        }
        return array; //returns a scrambled array
  } //end methd
} //end class