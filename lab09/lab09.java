//Tom Madigan
//CSE2

public class lab09{
  
  public static int[] copy(int[] array0){
 
    int[] copy = new int[array0.length];
      for (int i = 0; i < 10; i++)
    {
        copy[i] = array0[i];
    }
    return copy;
  }
  
  public static void inverter(int[] array1){
    
    int j = array1.length-1;
    for( int i = 0; i<array1.length/2; i++){
      int temp = array1[i];
      array1[i] = array1[j];
      array1[j] = temp;
      j--;
    }
   }
  
  public static int[] inverter2(int[] array0){
    
    int[] array1 = copy(array0);
    int j = array1.length-1;
    for( int i = 0; i<array1.length/2; i++){
      int temp = array1[i];
      array1[i] = array1[j];
      array1[j] = temp;
      j--;
    }
    return array1;
   }
 
  public static void print(int[] a){
      for (int i = 0; i < 10; i++){
      System.out.print(a[i]+" ");
    }
  }
  
  
  public static void main(String[] args){
    
    int arrayLength = 10; 
    
    int[] array0 = new int[arrayLength];
    array0[0]=0;
    array0[1]=1;
    array0[2]=2;
    array0[3]=3;
    array0[4]=4;
    array0[5]=5;
    array0[6]=6;
    array0[7]=7;
    array0[8]=8;
    array0[9]=9;
    
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    inverter(array0);
    print(array0);
    System.out.println();
    inverter2(array1);
    print(array1);
    System.out.println();
    int[] array3 = inverter2(array2);
    print(array3);
  
  }  
}