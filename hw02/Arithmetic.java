//Thomas Madigan
//9-6-2018
//CSE 002
public class Arithmetic {
  
  public static void main (String[]args){
    int numPants = 3; //number of pairs of pants
    double pantsPrice = 34.98; //cost per pair of pants
    int numShirts = 2; //number of sweatshirts
    double shirtPrice = 24.99;  //cost per shirt
    int numBelts = 1; //number of belts
    double beltPrice = 33.99; //cost per belt
    double paSalesTax = 0.06; //tax rate
    double pantsBeforeTax, shirtsBeforeTax, beltBeforeTax; //declares item costs before tax as variables
    double pantsTax, shirtTax, beltTax; //declares taxes on items as variables
    double pantsTotal, shirtTotal, beltTotal; //declares individual total costs as variables
    double totalBeforeTax, totalTax, totalFinal; //decalares combined total costs as variables
    
    pantsBeforeTax=numPants * pantsPrice; //computes pants cost before tax
    pantsTax=pantsBeforeTax * paSalesTax; //computes pants tax
    pantsTax=100 * pantsTax; 
    pantsTax= (int) pantsTax / 100.0; // rounds pants tax to nearest hundreth
    pantsTotal=numPants * pantsPrice + (pantsTax); //computes total pants cost 
    pantsTotal=Math.round (pantsTotal * 100.00) / 100.00; //rounds pants total to nearest hundreth
    
    shirtsBeforeTax=numShirts * shirtPrice; //computes shirt cost before tax
    shirtTax=shirtsBeforeTax * paSalesTax; //computes shirt tax
    shirtTax=100 * shirtTax;
    shirtTax= (int) shirtTax / 100.0; //rounds shirt tax to nearest hundreth
    shirtTotal=numShirts * shirtPrice + (shirtTax); //computes total shirt cost
    shirtTotal=Math.round (shirtTotal * 100.00) / 100.00; //rounds shirts total to neareast hundreth
    
    beltBeforeTax=numBelts * beltPrice; //computes belt cost before tax
    beltTax=beltBeforeTax * paSalesTax; //computes belt tax
    beltTax=100 * beltTax;
    beltTax = (int) beltTax / 100.0; //rounds belt tax to nearest hundreth
    beltTotal=numBelts * beltPrice + (beltTax); //rounds belt total to nearest hundreth
    beltTotal=Math.round (beltTotal * 100.00) / 100.00; //computes total belt cost
    
    totalBeforeTax=pantsBeforeTax + shirtsBeforeTax + beltBeforeTax; //computes total of all items before tax
    totalTax=pantsTax + shirtTax + beltTax; //computes total tax of all items
    totalFinal= pantsTotal + shirtTotal + beltTotal; //computes final total for complete purchase
    
    System.out.println("Cost of pants before tax = $"+pantsBeforeTax+" and sales tax on pants = $"+pantsTax+""); //displays calculations for pants
    System.out.println("Cost of shirts before tax = $"+shirtsBeforeTax+" and sales tax on shirts = $"+shirtTax+""); //displays calculations for shirts
    System.out.println("Cost of belts before tax = $"+beltBeforeTax+" and sales tax on belts = $"+beltTax+""); //displays calculations for belts
    System.out.println("Total purchase cost before tax = $"+totalBeforeTax+" total for all taxes + $"+totalTax+" and the total for the entire purchase = $"+totalFinal+"."); //displays all total calculations
  }  
}