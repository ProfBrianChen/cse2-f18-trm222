//Tom Madigan
//10-11-18
//Section 215
import java.util.Scanner;

public class PatternB {
  
  public static void main(String args[]) {
    Scanner input = new Scanner ( System.in );
    //creates variables used for intial inuot check
    String trash = "";
    int garb;
    int pyrSize = 0;
   
    
    System.out.print("Enter desired pyramid size from 1-10 rows: ");
    
    //creates loop that will repeat until an int from 1 to 10 is inputted
        while (input.hasNextInt()==false)
        {
          trash = input.next(); //non integers are stored and the user is prompted to enter another int
          System.out.print("Please enter and int from 1-10: ");
        }
     
      pyrSize = input.nextInt(); //stores correct input in a variable
      int numb; //creates vars for the pattern loops
      int numRows;
    
    //loops for creating pattern ,b according to the inputted size
   for(numRows = pyrSize; numRows >= 1; numRows--) {
            for(numb = 1; numb <= numRows; numb++) {
                System.out.print( numb + " ");
            }
            System.out.println();
    }
   }
}