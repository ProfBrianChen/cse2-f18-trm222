//Tom Madigan
//Section 210
import java.util.Scanner; //imports scanner

public class EncryptedX { //class
  
  public static void main (String args[]) {
    Scanner input = new Scanner ( System.in ); //creates scanner for input
    //variables declared for validation
    String trash = "";
    int garb = 0;
    int x;
    //prompts user to enter input 
    System.out.print("Enter desired x size from 1-100 rows: ");
    //creates loop that will repeat until correct input is inputted
     while (input.hasNextInt() == false) 
       {
          trash = input.next(); //non integers are stored and the user is prompted to enter another int
          System.out.print("Please enter an int from 1-100: "); //reprompts user to add input
       }
  
    x  = input.nextInt(); //assigns valid int to variable
      
      for (int i = 0; i < x; i++) //first two for loops make square shape
        {
	        for (int j = 0; j < x; j++) 
           {
	          if ( i == j || i + j == x - 1) //this loop makes the diagonals of the square into spaces
            {
	            System.out.print(" ");
            }
            else
            {
          System.out.print("*"); //all spaces but diagonals will be *
            }
	         }
        System.out.print("\n"); //creates new line
      
  } 
 } //end method
} //end class
    
    
	 